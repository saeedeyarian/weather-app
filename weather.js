const form= document.querySelector (".top-banner form");
const input= document.querySelector (".top-banner form input");
const list= document.querySelector (".show-weather");
const msg = document.querySelector (".msg")

//define api
const apiKey= "bfcb8a4d44b365b0ed134e62b7e0fab2";

form.addEventListener ("submit", e => {
    e.preventDefault();

    const inputValue= input.value ;
    const url=`http://api.openweathermap.org/data/2.5/weather?q=${inputValue}&appid=${apiKey}&units=metric`
    fetch (url)
        .then(Response => Response.json())
        .then ( data => {
            const {main, name, sys, weather}= data
    const icon = `https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${weather[0]["icon"]}.svg`

//creat element &html tag
        const li= document.createElement ("li");
        li.classList.add ("city-information");

        const markup=`
            <h2 class="city-name">
                <span>${name}</span>
                <span>${sys.country}</span>
            </h2>
            <div class="city-temp">${Math.round(main.temp)}</div>
            <figure>
                <img class="icon" src="${icon}">
                <figurecaption>${weather[0]["description"]}</figurecaption>
            </figure>
        `
        li.innerHTML= markup;
        list.appendChild (li);
        msg.innerText= "";
        })
        .catch ( () => {
            msg.innerText="search for a valid city";
        })

        input.value="";
})